<div class="row">
  <div class="col-md-2 ">
    <br><br><br>

        <center><img src="" alt=""></center>
        <div class="col-md-9">
          <!-- Button trigger modal -->
        </div>
        <br><br>

  </div>

  <div class="col-md-10">
    <br>
    <center><h1><i class="fa-solid fa-building-user"></i> LISTADO DE EDITORES</h1></center>
    <br>
    <a href="<?php echo site_url('editores/nuevo');?>" class="btn btn-danger">
      <i class="fa fa-plus-circle"></i> AGREGAR EDITORES
    </a>
    <div class="row">
      <div class="col-md-1"></div>
      <div class="col-md-10">
        <br>
        <?php if ($listadoEditores): ?>
          <table class="table table-bordered bg-light table-responsive text-center table-striped mi-tabla-personalizada">
            <thead class="table-dark">
              <tr>
                <th>ID</th>
                <th>ARTICULO</th>
                <th>NOMBRE</th>
                <th>APELLIDOS</th>
                <th>AREA</th>
                <th>ACCIONES</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($listadoEditores as $editor): ?>
                <tr>

                  <td><?php echo $editor->id_ed; ?></td>
                  <!--LLAMADO DE ARTICULO POR NOMBRE -->
                  <td><?php echo $this->Articulo->obtenerNombrePorId($editor->id_art); ?></td>
                  <td><?php echo $editor->nombre; ?></td>
                  <td><?php echo $editor->apellidos; ?></td>
                  <td><?php echo $editor->area; ?></td>

                  <td>
                    <a href="<?php echo site_url('editores/editar/') . $editor->id_ed; ?>" class="btn btn-outline-warning" title="Editar">EDITAR</a>
                    <a href="<?php echo site_url('editores/borrar/') . $editor->id_ed; ?>" class="btn btn-outline-danger" title="Borrar">ELIMINAR</a>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        <?php else: ?>
          <div class="alert alert-danger">
            No se encontraron agencias registradas
          </div>
        <?php endif; ?>
      </div>
      <div class="col-md-1"></div>
    </div>
  </div>
</div>
