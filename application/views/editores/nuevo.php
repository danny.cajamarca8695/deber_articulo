<div class="row">

  <div class="col-md-10">
    <div class="row">
      <div class="col-md-5">
        <br><br><br><br>
        <img src="" alt="" height="650px" >
      </div>
      <div class="col-md-7">
        <form class="formen" action="<?php echo site_url('editores/guardarEditor');?>" enctype="multipart/form-data" method="post" id="frm_editor">
          <br>
          <h1 class="text-center"><b><i class="fa-solid fa-plus"></i>   REGISTRO DE EDITORES</b></h1><br>

          <!-- SELECT DE ARTICULO -->
          <label for="id_art"><i class="fa-solid fa-circle-check"></i>   ARTICULO:</label>
          <br>
          <select name="id_art" id="id_art" class="form-control">
              <option value="">Seleccione un artículo</option>
              <?php foreach ($listado as $articulo): ?>
                  <option value="<?php echo $articulo->id_art; ?>"><?php echo $this->Articulo->obtenerNombrePorId($articulo->id_art); ?></option>
              <?php endforeach; ?>
          </select>
          <br>
          <!-- FIN -->

          <div class="row">
            <div class="col-md-12">
              <label for=""><i class="fa-solid fa-circle-check"></i>   NOMBRES</label>
            </i><input type="text" name="nombre" id="nombre" class="form-control" placeholder="Ingrese el nombre">
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-6">
              <label for=""><i class="fa-solid fa-circle-check"></i>   APELLIDOS:</label>
              <input type="text" name="apellidos" id="apellidos" class="form-control" placeholder="Ingrese la direccion">
            </div>
            <div class="col-md-6">
              <label for=""><i class="fa-solid fa-circle-check"></i>   AREA:</label>
              <input type="text" name="area" id="area" class="form-control" placeholder="Ingrese la ciudad">
            </div>
          </div>
          <br>

          <div class="row">
            <div class="col-md-12 text-center">
              <button type="submit" name="button" class="btn btn-dark btn-rounded"><i class="fa-regular fa-floppy-disk"></i>  GUARDAR</button>
              <a href="<?php echo site_url('autores/index');?>" class="btn btn-danger"><i class="fa-regular fa-trash-can"></i>  ELIMINAR</a>
            </div>
          </div>
        </form>

        <br><br>
        <script type="text/javascript">
          $.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-zA-Z\s]+$/.test(value);
          }, "DEBE CONTENER SOLO LETRAS");

          $("#frm_editor ").validate({
            rules:{
            "nombre": {
                required: true,
                lettersonly: true,
                minlength: 5,
                maxlength: 50
            },
            "apellidos": {
                required: true,
                lettersonly: true,

                minlength: 5,
                maxlength: 100
            },
            "area": {
                required: true,
                lettersonly: true,

                minlength: 2,
                maxlength: 50
            }
        },
        messages:{
            "nombre": {
                required: "DEBE INGRESAR EL NOMBRE",
                minlength: "EL NOMBRE DEBE TENER AL MENOS 5 CARACTERES",
                maxlength: "EL NOMBRE NO PUEDE TENER MÁS DE 50 CARACTERES"
            },
            "apellidos": {
                required: "DEBE INGRESAR LOS APELLIDOS",
                minlength: "LOS APELLIDOS DEBEN TENER AL MENOS 5 CARACTERES",
                maxlength: "LOS APELLIDOS DEBEN TENER MÁS 50 CARACTERES"
            },
            "area": {
                required: "DEBE INGRESAR ELA REA",
                minlength: "EL AREA DEBE TENER AL MENOS 2 CARACTERES",
                maxlength: "EL AREA NO PUEDE TENER MÁS DE 50 CARACTERES"
            }
        }
    });

        </script>
      </div>
    </div>
  </div>
  <div class="col-md-1"></div>
</div>
