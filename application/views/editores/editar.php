<div class="row">

  <div class="col-md-10">
    <div class="row">
      <div class="col-md-5">
        <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-8">
            <br><br><br><br><br><br>
            <img src="" alt="" height="550px">
          </div>
        </div>
      </div>
      <div class="col-md-7">
        <form id="frm_editor" class="formen mb-5" method="post" enctype="multipart/form-data" action="<?php echo site_url('editores/actualizarEditor'); ?>">
          <br>
          <h1 class="text-center"><b><i class="fa-solid fa-file-pen"></i></i>   EDITAR EDITOR</b></h1><br>
          <input type="hidden" name="id_ed" id="id_ed" value="<?php echo $editorEditar->id_ed; ?>">

          <!--SELECT ARTICULO -->
          <label for=""><b><i class="fa-solid fa-circle-check"></i>   ARTICULO:</b></label>
          <br>
          <select name="id_art" id="id_art" class="form-control" required>
            <?php foreach ($articulos as $articulo) : ?>
              <option value="<?php echo $articulo->id_art; ?>" <?php echo ($articulo->id_art == $editorEditar->id_art) ? 'selected' : ''; ?>><?php echo $articulo->nombre; ?></option>
            <?php endforeach; ?>
          </select>
          <!--FIN-->


          <div class="form-group">
            <label for="nombres"><b><i class="fa-solid fa-circle-check"></i>   NOMBRE:</b></label>
            <input type="text" name="nombre" id="nombre" value="<?php echo $editorEditar->nombre; ?>" placeholder="Ingrese el nombre..." class="form-control" required>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="apellidos"><b><i class="fa-solid fa-circle-check"></i>  APELLIDOS:</b></label>
                <input type="text" name="apellidos" id="apellidos" value="<?php echo $editorEditar->apellidos; ?>" placeholder="Ingrese la direccion.." class="form-control" required>
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label for="area"><b><i class="fa-solid fa-circle-check"></i>   AREA:</b></label>
                <input type="text" name="area" id="area" value="<?php echo $editorEditar->area; ?>" placeholder="Ingrese la ciudad..." class="form-control" required>
              </div>
            </div>
          </div>



          <br>
          <div class="row">
            <div class="col-md-12 text-center">
              <button type="submit" name="button" class="btn btn-warning"><i class="fa fa-pen fa-bounce"></i> &nbsp ACTUALIZAR</button> &nbsp &nbsp
              <a href="<?php echo site_url('editores/index'); ?>" class="btn btn-danger"> <i class="fa fa-xmark fa-spin"></i> &nbsp CANCELAR</a>
            </div>
          </div>
        </form>

        <script type="text/javascript">
          $.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-zA-Z\s]+$/.test(value);
          }, "DEBE CONTENER SOLO LETRAS");

          $("#frm_editor ").validate({
            rules:{
            "nombre": {
                required: true,
                lettersonly: true,
                minlength: 5,
                maxlength: 50
            },
            "apellidos": {
                required: true,
                lettersonly: true,

                minlength: 5,
                maxlength: 100
            },
            "area": {
                required: true,
                lettersonly: true,

                minlength: 2,
                maxlength: 50
            }
        },
        messages:{
            "nombre": {
                required: "DEBE INGRESAR EL NOMBRE",
                minlength: "EL NOMBRE DEBE TENER AL MENOS 5 CARACTERES",
                maxlength: "EL NOMBRE NO PUEDE TENER MÁS DE 50 CARACTERES"
            },
            "apellidos": {
                required: "DEBE INGRESAR LOS APELLIDOS",
                minlength: "LOS APELLIDOS DEBEN TENER AL MENOS 5 CARACTERES",
                maxlength: "LOS APELLIDOS DEBEN TENER MÁS 50 CARACTERES"
            },
            "area": {
                required: "DEBE INGRESAR ELA REA",
                minlength: "EL AREA DEBE TENER AL MENOS 2 CARACTERES",
                maxlength: "EL AREA NO PUEDE TENER MÁS DE 50 CARACTERES"
            }
        }
    });

        </script>
      </div>
    </div>
  </div>
  <div class="col-md-1"></div>
</div>
