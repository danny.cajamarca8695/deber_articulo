<div class="row">

  <div class="col-md-10">
    <br><br>
    <div class="row mb-5">
      <div class="col-md-5">
      </div>
      <div class="col-md-7">
        <form class="formen" action="<?php echo site_url('autores/guardarAutor');?>" enctype="multipart/form-data" method="post" id="frm_autor">
          <h1 class="text-center"><b><i class="fa-solid fa-plus"></i>   REGISTRO DE AUTORES</b></h1><br>
          <div class="row">
            <div class="col-md-12">
              <label for=""><i class="fa-solid fa-circle-check"></i>   NOMBRES</label>
            </i><input type="text" name="nombres" id="nombres" class="form-control" placeholder="Ingrese el nombre">
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-6">
              <label for=""><i class="fa-solid fa-circle-check"></i>   APELLIDOS:</label>
              <input type="text" name="apellidos" id="apellidos" class="form-control" placeholder="Ingrese los apellidos">
            </div>
            <div class="col-md-6">
              <label for=""><i class="fa-solid fa-circle-check"></i>   AFILIACION:</label>
              <input type="text" name="afiliacion" id="afiliacion" class="form-control" placeholder="Ingrese la afiliacion">
            </div>
          </div>
          <br>
          <div class="col-md-6">
            <label for=""><i class="fa-solid fa-circle-check"></i>   CORREO:</label>
            <input type="email" name="correo" id="correo" class="form-control" placeholder="Ingrese el correo">
          </div>
        </div>
        <br>

          <div class="row">
            <div class="col-md-12 text-center">
              <br>
              <button type="submit" name="button" class="btn btn-dark btn-rounded"><i class="fa-regular fa-floppy-disk"></i>  GUARDAR</button>
              <a href="<?php echo site_url('autores/index');?>" class="btn btn-danger"><i class="fa-regular fa-trash-can"></i>  ELIMINAR</a>
            </div>
          </div>
        </form>

        <br><br>
        <script type="text/javascript">
                $.validator.addMethod("lettersonly", function(value, element) {
                  return this.optional(element) || /^[a-zA-Z\s]+$/.test(value);
                }, "DEBE CONTENER SOLO LETRAS");

                $("#frm_autor").validate({
                  rules:{
                  "nombres": {
                      required: true,
                      lettersonly: true,
                      minlength: 5,
                      maxlength: 50
                  },
                  "apellidos": {
                      required: true,
                      lettersonly: true,
                      minlength: 5,
                      maxlength: 50
                  },
                  "afiliacion": {
                      required: true,
                      lettersonly: true,
                      minlength: 2,
                      maxlength: 50
                  },
                  "correo": {
                      required: true,
                      email: true
                  }
              },
              messages:{
                  "nombres": {
                      required: "DEBE INGRESAR LOS NOMBRES",
                      minlength: "LOS NOMBRE DEBEN TENER AL MENOS 5 CARACTERES",
                      maxlength: "LOS NOMBRES NO PUEDEN TENER MÁS DE 50 CARACTERES"
                  },
                  "apellidos": {
                      required: "DEBE INGRESAR LOS APELLIDOS",
                      minlength: "LOS APELLIDOS DEBEN TENER AL MENOS 5 CARACTERES",
                      maxlength: "LOS APELLIDOS DEBEN TENER MÁS 50 CARACTERES"
                  },
                  "afiliacion": {
                      required: "DEBE INGRESAR LA AFILIACION",
                      minlength: "LA CIUDAD DEBE TENER AL MENOS 2 CARACTERES",
                      maxlength: "LA CIUDAD NO PUEDE TENER MÁS DE 50 CARACTERES"
                  },

                  "correo": {
                      required: "DEBE INGRESAR EL CORREO ELECTRÓNICO",
                      email: "INGRESE UN CORREO ELECTRÓNICO VÁLIDO"
                  }
              }
          });

        </script>
      </div>
    </div>
  </div>
  <div class="col-md-1"></div>
</div>
