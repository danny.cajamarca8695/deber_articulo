<div class="row">
    <div class="col-md-2">
        <br><br><br>

    </div>


    <div class="col-md-10">
    }
        <center><h2><i class="fa-solid fa-building-user"></i> LISTADO DE AUTORES</h2></center>
        <a href="<?php echo site_url('autores/nuevo'); ?>" class="btn btn-danger">
            <i class="fa fa-plus-circle"></i> AGREGAR AUTOR
        </a>
        <br><br>
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <?php if (isset($listadoAutores) && $listadoAutores): ?>
                    <table class="table table-bordered bg-light table-responsive text-center table-striped mi-tabla-personalizada">
                        <thead class="table-dark">
                            <tr>
                                <th>ID</th>
                                <th>NOMBRE</th>
                                <th>APELLIDOS</th>
                                <th>AFILIACIÓN</th>
                                <th>CORREO</th>
                                <th>ACCIONES</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($listadoAutores as $autor): ?>
                                <tr>
                                    <td><?php echo $autor->id_au; ?></td>
                                    <td><?php echo $autor->nombres; ?></td>
                                    <td><?php echo $autor->apellidos; ?></td>
                                    <td><?php echo $autor->afiliacion; ?></td>
                                    <td><?php echo $autor->correo; ?></td>
                                    <td>
                                        <a href="<?php echo site_url('autores/editar/') . $autor->id_au; ?>" class="btn btn-outline-warning" title="Editar">ACTUALIZAR</a>
                                        <a href="<?php echo site_url('autores/borrar/') . $autor->id_au; ?>" class="btn btn-outline-danger" title="Borrar">ELIMINAR</a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php else: ?>
                    <div class="alert alert-danger">
                        No se encontraron autores registrados
                    </div>
                <?php endif; ?>
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>
</div>
