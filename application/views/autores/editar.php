<div class="row">

  <div class="col-md-10">

    <div class="row">
      <div class="col-md-5">
        <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-8">
          </div>
        </div>
      </div>

      <div class="col-md-7">
        <form id="frm_autor" class="formen mb-5" method="post" enctype="multipart/form-data" action="<?php echo site_url('autores/actualizarAutor'); ?>">
          <br>
          <h1 class="text-center"><b><i class="fa-solid fa-file-pen"></i></i>   EDITAR AUTOR</b></h1><br>
          <input type="hidden" name="id_au" id="id_au" value="<?php echo $autorEditar->id_au; ?>">

          <div class="form-group">
            <label for="nombres"><b><i class="fa-solid fa-circle-check"></i>   NOMBRE:</b></label>
            <input type="text" name="nombres" id="nombres" value="<?php echo $autorEditar->nombres; ?>" placeholder="Ingrese el nombre..." class="form-control" required>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="apellidos"><b><i class="fa-solid fa-circle-check"></i>  APELLIDOS:</b></label>
                <input type="text" name="apellidos" id="apellidos" value="<?php echo $autorEditar->apellidos; ?>" placeholder="Ingrese los apellidos.." class="form-control" required>
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label for="afiliacion"><b><i class="fa-solid fa-circle-check"></i>   AFILIACION:</b></label>
                <input type="text" name="afiliacion" id="afiliacion" value="<?php echo $autorEditar->afiliacion; ?>" placeholder="Ingrese la afiliacion..." class="form-control" required>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="correo"><b><i class="fa-solid fa-circle-check"></i>   CORREO:</b></label>
                <input type="text" name="correo" id="correo" value="<?php echo $autorEditar->correo; ?>" placeholder="Ingrese el correo..." class="form-control" required>
              </div>
            </div>


          <br>
          <div class="row">
            <div class="col-md-12 text-center">
              <br>
              <button type="submit" name="button" class="btn btn-warning"><i class="fa fa-pen fa-bounce"></i> &nbsp ACTUALIZAR</button> &nbsp &nbsp
              <a href="<?php echo site_url('autores/index'); ?>" class="btn btn-danger"> <i class="fa fa-xmark fa-spin"></i> &nbsp CANCELAR</a>
            </div>
          </div>
        </form>

        <script type="text/javascript">
                $.validator.addMethod("lettersonly", function(value, element) {
                  return this.optional(element) || /^[a-zA-Z\s]+$/.test(value);
                }, "DEBE CONTENER SOLO LETRAS");

                $("#frm_autor").validate({
                  rules:{
                  "nombres": {
                      required: true,
                      lettersonly: true,
                      minlength: 5,
                      maxlength: 50
                  },
                  "apellidos": {
                      required: true,
                      lettersonly: true,
                      minlength: 5,
                      maxlength: 50
                  },
                  "afiliacion": {
                      required: true,
                      lettersonly: true,
                      minlength: 2,
                      maxlength: 50
                  },
                  "correo": {
                      required: true,
                      email: true
                  }
              },
              messages:{
                  "nombres": {
                      required: "DEBE INGRESAR LOS NOMBRES",
                      minlength: "LOS NOMBRE DEBEN TENER AL MENOS 5 CARACTERES",
                      maxlength: "LOS NOMBRES NO PUEDEN TENER MÁS DE 50 CARACTERES"
                  },
                  "apellidos": {
                      required: "DEBE INGRESAR LOS APELLIDOS",
                      minlength: "LOS APELLIDOS DEBEN TENER AL MENOS 5 CARACTERES",
                      maxlength: "LOS APELLIDOS DEBEN TENER MÁS 50 CARACTERES"
                  },
                  "afiliacion": {
                      required: "DEBE INGRESAR LA AFILIACION",
                      minlength: "LA CIUDAD DEBE TENER AL MENOS 2 CARACTERES",
                      maxlength: "LA CIUDAD NO PUEDE TENER MÁS DE 50 CARACTERES"
                  },

                  "correo": {
                      required: "DEBE INGRESAR EL CORREO ELECTRÓNICO",
                      email: "INGRESE UN CORREO ELECTRÓNICO VÁLIDO"
                  }
              }
          });

        </script>
      </div>
    </div>
  </div>
  <div class="col-md-1"></div>
</div>
