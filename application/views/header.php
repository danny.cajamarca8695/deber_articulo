<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Modernize Free</title>
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url('assets/images/logos/favicon.png'); ?>" />
    <!-- Cambia las rutas a 'static' -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/styles.min.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/librerias/izitoast/iziToast.min.css'); ?>" />
    <!-- Incluye Select2 CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/librerias/select2/select2.min.css'); ?>">
    <style media="screen">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<!-- Asegúrate de incluir jQuery Validation -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js"></script>

        /* stylo jquery validate */
        label.error {
            color: red;
            font-weight: bold;
        }

        input.error,
        select.error,
        textarea.error {
            border: 1px solid red;
        }
    </style>

</head>

<body>

    <!--  Body Wrapper -->
    <div class="page-wrapper" id="main-wrapper" data-layout="vertical" data-navbarbg="skin6"
        data-sidebartype="full" data-sidebar-position="fixed" data-header-position="fixed">
        <!-- Sidebar Start -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div>
                <div class="brand-logo d-flex align-items-center justify-content-between">
                    <a href="./index.html" class="text-nowrap logo-img">
                        <img src="../assets/images/logos/dark-logo.svg" width="180" alt="" />
                    </a>
                    <div class="close-btn d-xl-none d-block sidebartoggler cursor-pointer" id="sidebarCollapse">
                        <i class="ti ti-x fs-8"></i>
                    </div>
                </div>
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav scroll-sidebar" data-simplebar="">
                    <ul id="sidebarnav">
                        <li class="nav-small-cap">
                            <i class="ti ti-dots nav-small-cap-icon fs-4"></i>
                            <span class="hide-menu">Home</span>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="<?php echo site_url('tipos/index'); ?>"
                                aria-expanded="false">
                                <span>
                                    <i class="ti ti-layout-dashboard"></i>
                                </span>
                                <span class="hide-menu">Tipos</span>
                            </a>
                            <a class="sidebar-link" href="<?php echo site_url('articulos/index'); ?>"
                                aria-expanded="false">
                                <span>
                                    <i class="ti ti-layout-dashboard"></i>
                                </span>
                                <span class="hide-menu">Articulo</span>
                            </a>


            <a class="nav-link" href="<?php echo site_url('editoriales/index');?>">Editorial</a>
            <a class="sidebar-link" href="<?php echo site_url('coautores/index'); ?>"
                aria-expanded="false">
                <span>
                    <i class="ti ti-layout-dashboard"></i>
                </span>
                <span class="hide-menu">Coautor</span>
            </a>
            <a class="sidebar-link" href="<?php echo site_url('revisores/index'); ?>"
                aria-expanded="false">
                <span>
                    <i class="ti ti-layout-dashboard"></i>
                </span>
                <span class="hide-menu">Revisor</span>
            </a>

          </li>


            <a class="nav-link" href="<?php echo site_url('revistas/index');?>">Revista</a>
            <a class="sidebar-link" href="<?php echo site_url('autores/index'); ?>"
                                aria-expanded="false">
                                <span>
                                    <i class="ti ti-layout-dashboard"></i>
                                </span>
                                <span class="hide-menu">AUTORES</span>
                            </a>
                            <a class="sidebar-link" href="<?php echo site_url('editores/index'); ?>"
                                aria-expanded="false">
                                <span>
                                    <i class="ti ti-layout-dashboard"></i>
                                </span>
                                <span class="hide-menu">EDITORES</span>
                            </a>
                        </li>
                        <li class="nav-small-cap">
                            <i class="ti ti-dots nav-small-cap-icon fs-4"></i>
                            <span class="hide-menu">Categoria</span>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="<?php echo site_url('listadoCategorias'); ?>"
                                aria-expanded="false">
                                <span>
                                    <i class="ti ti-article"></i>
                                </span>
                                <span class="hide-menu">CATEGORIA</span>
                            </a>
                        </li>
                        <li class="nav-small-cap">
                            <i class="ti ti-dots nav-small-cap-icon fs-4"></i>
                            <span class="hide-menu">Inscripciones</span>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="<?php echo site_url('listadoInscripciones'); ?>"
                                aria-expanded="false">
                                <span>
                                    <i class="ti ti-login"></i>
                                </span>
                                <span class="hide-menu">INSCRIPCIONES</span>
                            </a>

                    </ul>
                    <div class="unlimited-access hide-menu bg-light-primary position-relative mb-7 mt-5 rounded">

                    </div>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!--  Sidebar End -->
        <!--  Main wrapper -->
        <div class="body-wrapper">
            <!--  Header Start -->
            <header class="app-header">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <ul class="navbar-nav">
                        <li class="nav-item d-block d-xl-none">
                            <a class="nav-link sidebartoggler nav-icon-hover" id="headerCollapse"
                                href="javascript:void(0)">
                                <i class="ti ti-menu-2"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-icon-hover" href="javascript:void(0)">
                                <i class="ti ti-bell-ringing"></i>
                                <div class="notification bg-primary rounded-circle"></div>
                            </a>
                        </li>
                    </ul>
                    <div class="navbar-collapse justify-content-end px-0" id="navbarNav">
                        <ul class="navbar-nav flex-row ms-auto align-items-center justify-content-end">
                            <a href="https://adminmart.com/product/modernize-free-bootstrap-admin-dashboard/"
                                target="_blank" class="btn btn-primary">Download Free</a>
                            <li class="nav-item dropdown">
                                <a class="nav-link nav-icon-hover" href="javascript:void(0)" id="drop2"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    <img src="/assets/images/profile/user-1.jpg" alt="" width="35" height="35"
                                        class="rounded-circle">
                                </a>
                                <div class="dropdown-menu dropdown-menu-end dropdown-menu-animate-up"
                                    aria-labelledby="drop2">
                                    <div class="message-body">
                                        <a href="javascript:void(0)"
                                            class="d-flex align-items-center gap-2 dropdown-item">
                                            <i class="ti ti-user fs-6"></i>
                                            <p class="mb-0 fs-3">My Profile</p>
                                        </a>
                                        <a href="javascript:void(0)"
                                            class="d-flex align-items-center gap-2 dropdown-item">
                                            <i class="ti ti-mail fs-6"></i>
                                            <p class="mb-0 fs-3">My Account</p>
                                        </a>
                                        <a href="javascript:void(0)"
                                            class="d-flex align-items-center gap-2 dropdown-item">
                                            <i class="ti ti-list-check fs-6"></i>
                                            <p class="mb-0 fs-3">My Task</p>
                                        </a>
                                        <a href="<?php echo site_url('logout'); ?>"
                                            class="btn btn-outline-primary mx-3 mt-2 d-block">Logout</a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!--  Header End -->
            <div class="py-6 px-6 text-center">
                <p class="mb-0 fs-4">Design and Developed by <a href="https://adminmart.com/" target="_blank"
                        class="pe-1 text-primary text-decoration-underline">AdminMart.com</a> Distributed by <a
                        href="https://themewagon.com">ThemeWagon</a></p>
            </div>
        </div>

        <?php if (!empty($messages)) : ?>
        <script type="text/javascript">
            document.addEventListener('DOMContentLoaded', function () {
                <?php foreach ($messages as $mensaje) : ?>
                iziToast.success({
                    title: '¡CONFIRMACIÓN!',
                    message: '<?php echo $mensaje; ?>',
                    position: 'topRight'
                });
                <?php endforeach; ?>
            });
        </script>
        <?php endif; ?>

        <!-- Carga de archivos estáticos de Django -->
        <!-- <script src="<?php echo base_url('assets/librerias/jquery/jquery.min.js'); ?>"></script> -->
        <script src="<?php echo base_url('assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/sidebarmenu.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/app.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/libs/simplebar/dist/simplebar.js'); ?>"></script>
        <script src="<?php echo base_url('assets/librerias/izitoast/iziToast.min.js'); ?>"></script>
        <!-- Incluye Select2 JS -->
        <script src="<?php echo base_url('assets/librerias/select2/select2.min.js'); ?>"></script>

</body>

</html>
