<div class="row">
<div class="col-md-4 ">

</div>
<div class="col-md-8 ">
  <br>
  <br>
  <a href="<?php echo site_url('tipos/nuevo')?>" class="btn btn-secondary">NUEVO</a>
  <br>
  <br>
  <?php if ($listado): ?>
  <table class="table table-bordered">

    <thead>
      <tr>
        <td>ID</td>
        <td>NOMBRE</td>
        <td>ACCIONES</td>
        </tr>
    </thead>
    <tbody>
      <?php foreach ($listado as $tipo): ?>
        <tr>
          <td><?php echo $tipo->id; ?></td>
          <td><?php echo $tipo->tipo; ?></td>
          <td>
            <a href="<?php echo site_url('tipos/editar/').$tipo->id; ?>" class="btn btn-warning">EDITAR</a>
            <a href="<?php echo site_url('tipos/borrar/').$tipo->id; ?>" class="btn btn-danger"> ELIMINAR</a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
  <!-- Button trigger modal -->


  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">MAPA HOSPITALES</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div id="reporteMapa" style="height:300px; width:100%; border:2px solid black;" class="">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Cerrar</button>
        </div>
        </div>
      </div>
    </div>

    <script type="text/javascript">
      function initMap(){
        var coordenadaCentral=new google.maps.LatLng(-0.152948869329262,-78.4868431364856);
        var miMapa=new google.maps.Map(
          document.getElementById('reporteMapa'),
          {
            center:coordenadaCentral,
            zoom:8,
            mapTypeId:google.maps.MapTypeId.ROADMAP
          }
        );
        <?php foreach ($listadoHospitales as $hospital): ?>
        var coordenadaTemporal=
            new google.maps.LatLng(
              <?php echo $hospital->latitud_hos; ?>,
              <?php echo $hospital->longitud_hos; ?>);
          var marcador=new google.maps.Marker({
            position:coordenadaTemporal,
            map:miMapa,
            title:'<?php echo $hospital->nombre_hos; ?>',
          });
        <?php endforeach; ?>

      }
    </script>
<?php else: ?>

  <div class="alert alert-danger">
    No se encontraron registros en la tabla hospitales

  </div>

<?php endif; ?>


</div>
</div>
