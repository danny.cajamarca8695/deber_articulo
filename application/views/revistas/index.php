
<div class="row">
<div class="col-md-4 ">

</div>
<div class="col-md-8 ">
  <br>
  <br>
<h1>
<i class="fa fa-hospital"></i>
REVISTAS
</h1>
<div class="row">
  <div class="col-md-12 text-end" >



    <a href="<?php echo site_url('revistas/nuevo');?>" class="btn btn-outline-success">
     <i class="fa fa-plus-circle"></i>
     AGREGAR REVISTAS
  </a>
  <br><br>
  </div>
</div>
<?php if ($listadoRevistas): ?>
    <table class="table table-bordered">
      <thead>
        <tr>
          <th>ID</th>
          <th>NOMBRE</th>
          <th>ISSN</th>
          <th>FACTOR IMPACTO</th>


          <th>ACCIONES</th>

        </tr>
      </thead>
      <tbody>
        <?php foreach ($listadoRevistas as $revista): ?>
          <tr>
            <td> <?php echo $revista->id_rev; ?> </td>
            <td> <?php echo $revista->nombre; ?> </td>
            <td> <?php echo $revista->issn; ?> </td>
            <td> <?php echo $revista->factor_impacto; ?> </td>








            <td>

              <a href="<?php echo site_url('revistas/editar/').$revista->id_rev; ?>"
                   class="btn btn-warning"
                   title="Editar">
                <i class="fa fa-pen"></i>
              </a>



              <a href="<?php echo site_url('revistas/borrar/').$revista->id_rev; ?>"
                   class="btn btn-danger"
                   title="Borrar">
                <i class="fa fa-pen"></i>
              </a>
          </td>

          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>



<?php else: ?>
  <div class="alert alert-danger">
      No se encontro revistas registrados
  </div>
<?php endif; ?>
