
<div class="row">
<div class="col-md-4 ">

</div>
<div class="col-md-8 ">
  <br>
  <br>
<h1>EDITAR REVISTA</h1>
<form class="" method="post" action="<?php echo site_url('revistas/actualizarRevista'); ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_revista" >
	<input type="hidden" name="id_rev" id="id_rev"
	value="<?php echo $revistaEditar->id_rev; ?>">
  <label for="">
    <b>nombre:</b>
  </label>
  <input type="text" name="nombre" id="nombre"
	value="<?php echo $revistaEditar->nombre; ?>"
  placeholder="Ingrese el nombre..." class="form-control" required>
  <br>
  <label for="">
    <b>issn:</b>
  </label>
  <input type="text" name="issn" id="issn"
	value="<?php echo $revistaEditar->issn; ?>"
  placeholder="Ingrese la issn..." class="form-control" required>
  <br>
  <label for="">
    <b>Factor impacto:</b>
  </label>
  <input type="text" name="factor_impacto" id="factor_impacto"
	value="<?php echo $revistaEditar->factor_impacto; ?>"
  placeholder="Ingrese el factor_impacto..." class="form-control" required>





    </div>
    <div class="row">
      <div class="col-md-12">
        <div id="mapa" style="height: 250px; whidth:100%; border:0px solid black;">

      </div>
      </div>

    </div>
    <br>
    <br>
    <div class="row">
      <div class="col-md-12 text-center">
        <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-pen"></i> &nbsp ACTUALIZAR</button> &nbsp &nbsp
        <a href="<?php echo site_url('revistas/index'); ?>" class="btn btn-danger"> <i class="fa fa-xmark fa-spin"></i> &nbsp Cancelar</a>

      </div>

    </div>
</form>

<br>
<br>
<script type="text/javascript">
    $(document).ready(function(){
        $("#frm_nuevo_revista").validate({
            rules: {
                "nombre": {
                    required: true,
                    minlength: 2,
                    maxlength: 50,
                    lettersonly: true // Asegúrate de agregar esta regla personalizada
                },
                "issn": {
                    required: true,
                    minlength: 8,
                    maxlength: 8,
                    number: true // issn debe ser un número
                },
                "factor_impacto": {
                    required: true,
                    number: true // Asegúrate de que factor_impacto sea numérico
                }
            },
            messages: {
                "nombre": {
                    required: "Debes ingresar el nombre de la revista.",
                    minlength: "El nombre debe tener al menos 2 caracteres.",
                    maxlength: "El nombre no puede tener más de 50 caracteres.",
                    lettersonly: "El nombre solo puede contener letras."
                },
                "issn": {
                    required: "Debes ingresar el issn de la revista.",
                    minlength: "El issn debe tener exactamente 8 caracteres.",
                    maxlength: "El issn debe tener exactamente 8 caracteres.",
                    number: "El issn debe ser un número."
                },
                "factor_impacto": {
                    required: "Debes ingresar el factor de impacto de la revista.",
                }
            }
        });
    });
</script>
