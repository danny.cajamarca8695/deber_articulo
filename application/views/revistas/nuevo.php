
<div class="row">
<div class="col-md-4 ">

</div>
<div class="col-md-8 ">
  <br>
  <br>
<h1>
  <b>
  <i class="fa fa-plus-circle"></i>
  NUEVA REVISTA
  </b>
</h1>
<br>
<form action="<?php echo site_url('revistas/guardarRevista');?>" method="post" enctype="multipart/form-data" id="frm_nuevo_revista" >
  <label for="nombre" class="error-message">nombre:</label>
  <input type="text" name="nombre" id="nombre" class="form-control" placeholder="Ingrese el nombre">
  <br>

  <label for="issn" class="error-message">issn:</label>
  <input type="text" name="issn" id="issn" class="form-control" placeholder="Ingrese la dirección">
  <br>

  <label for="factor_impacto" class="error-message">factor_impacto:</label>
  <input type="text" name="factor_impacto" id="factor_impacto" class="form-control" placeholder="Ingrese el teléfono">
  <br>


  </div>

</div>

<div class="row">
  <div class="col-md-12">
    <br>
    <div id="mapa" style="height:200px; width: 100%; border:opx solid black;"></div>
  </div>
</div>
<br>

    <div class="row">
      <div class="cold-md-12 text-center">
        <button type="submit" name="button" class="btn btn-primary"> <i class="fa-solid fa-floppy-disk"></i> GUARDAR</button>
        <a href="<?php echo site_url('revistas/index');?>" class="btn btn-danger"> <i class="fa-solid fa-ban"></i>
          CANCELAR
        </a>
      </div>
    </div>


</form>
<br><br>






<script type="text/javascript">
    $(document).ready(function(){
        $("#frm_nuevo_revista").validate({
            rules:{
                "nombre": {
                    required: true,
                    lettersonly: true,
                    minlength: 2,
                    maxlength: 50
                },
                "issn": {
                    required: true,
                    minlength: 5,
                    maxlength: 100
                },
                "factor_impacto": {
                    required: true
                },

            },
            messages:{
                "nombre": {
                    required: "DEBE INGRESAR EL nombre DE LA REVISTA",
                    lettersonly: "DEBE CONTENER SOLO LETRAS",
                    minlength: "EL nombre DEBE TENER AL MENOS 2 CARACTERES",
                    maxlength: "EL nombre NO PUEDE TENER MÁS DE 50 CARACTERES"
                },
                "issn": {
                    required: "DEBE INGRESAR EL issn",
                    minlength: "LA DIRECCIÓN DEBE TENER AL MENOS 5 CARACTERES",
                    maxlength: "LA DIRECCIÓN NO PUEDE TENER MÁS DE 100 CARACTERES"
                },
                "factor_impacto": {
                    required: "DEBE INGRESAR EL FACTOR DE IMPACTO"
                }
            }
        });
    });
</script>
