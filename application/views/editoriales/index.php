
<div class="row">
<div class="col-md-4 ">

</div>
<div class="col-md-8 ">
  <br>
  <br>
  <br>
  <br>
  <a href="<?php echo site_url('editoriales/nuevo');?>" class="btn btn-outline-success">
   <i class="fa fa-plus-circle"></i>
   AGREGAR EDITORIALES
  </a>
  <br><br>
  <?php if ($listadoEditoriales): ?>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>ID</th>
        <th>NOMBRE</th>
        <th>DIRECCION</th>
        <th>CONTACTO</th>
        <th>SITIO WEB</th>
        <th>ACCIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoEditoriales as $editorial): ?>
        <tr>
          <td> <?php echo $editorial->id_edi; ?> </td>
          <td> <?php echo $editorial->nombre; ?> </td>
          <td> <?php echo $editorial->direccion; ?> </td>
          <td> <?php echo $editorial->contacto; ?> </td>
          <td> <?php echo $editorial->sitio_web; ?> </td>
          <td>

            <a href="<?php echo site_url('editoriales/editar/').$editorial->id_edi; ?>"
                 class="btn btn-warning"
                 title="Editar">
              <i class="fa fa-pen"></i>
            </a>



            <a href="<?php echo site_url('editoriales/borrar/').$editorial->id_edi; ?>"
                 class="btn btn-danger"
                 title="Borrar">
              <i class="fa fa-pen"></i>
            </a>
        </td>

        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>



  <?php else: ?>
  <div class="alert alert-danger">
    No se encontro hospitales registrados
  </div>
  </div>

  <?php endif; ?>


</div>
</div>
