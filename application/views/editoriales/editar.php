
<div class="row">
<div class="col-md-4 ">

</div>
<div class="col-md-8 ">
  <br>
  <br>
<h1>EDITAR EDITORIAL</h1>
<form class="" method="post" action="<?php echo site_url('editoriales/actualizarEditorial'); ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_editorial" >
	<input type="hidden" name="id_edi" id="id_edi"
	value="<?php echo $editorialEditar->id_edi; ?>">
  <label for="">
    <b>nombre:</b>
  </label>
  <input type="text" name="nombre" id="nombre"
	value="<?php echo $editorialEditar->nombre; ?>"
  placeholder="Ingrese el nombre..." class="form-control" required>
  <br>
  <label for="">
    <b>direccion:</b>
  </label>
  <input type="text" name="direccion" id="direccion"
	value="<?php echo $editorialEditar->direccion; ?>"
  placeholder="Ingrese la direccion..." class="form-control" required>
  <br>
  <label for="">
    <b>contacto:</b>
  </label>
  <input type="text" name="contacto" id="contacto"
	value="<?php echo $editorialEditar->contacto; ?>"
  placeholder="Ingrese el contacto..." class="form-control" required>

  <br>
  <label for="">
    <b>Sitio web:</b>
  </label>
  <input type="text" name="sitio_web" id="sitio_web"
	value="<?php echo $editorialEditar->sitio_web; ?>"
  placeholder="Ingrese el sitio web..." class="form-control" required>



    </div>
    <div class="row">
      <div class="col-md-12">
        <div id="mapa" style="height: 100px; whidth:100%; border:0px solid black;">

      </div>
      </div>

    </div>
    <br>
    <br>
    <div class="row">
      <div class="col-md-12 text-center">
        <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-pen"></i> &nbsp ACTUALIZAR</button> &nbsp &nbsp
        <a href="<?php echo site_url('editoriales/index'); ?>" class="btn btn-danger"> <i class="fa fa-xmark fa-spin"></i> &nbsp Cancelar</a>

      </div>

    </div>
</form>

<br>
<br>
<script type="text/javascript">
    $(document).ready(function(){
        $("#frm_nuevo_editorial").validate({
            rules:{
                "nombre": {
                    required: true,
                    lettersonly: true,
                    minlength: 2,
                    maxlength: 50
                },
                "direccion": {
                    required: true,
                    minlength: 5,
                    maxlength: 100
                },
                "contacto": {
                    required: true
                },
                "sitio_web": {
                    required: true
                }
            },
            messages:{
                "nombre": {
                    required: "DEBE INGRESAR EL nombre DE LA EDITORIAL",
                    lettersonly: "DEBE CONTENER SOLO LETRAS",
                    minlength: "EL nombre DEBE TENER AL MENOS 2 CARACTERES",
                    maxlength: "EL nombre NO PUEDE TENER MÁS DE 50 CARACTERES"
                },
                "direccion": {
                    required: "DEBE INGRESAR LA DIRECCIÓN DE LA EDITORIAK",
                    minlength: "LA DIRECCIÓN DEBE TENER AL MENOS 5 CARACTERES",
                    maxlength: "LA DIRECCIÓN NO PUEDE TENER MÁS DE 100 CARACTERES"
                },
                "contacto": {
                    required: "DEBE INGRESAR EL TELÉFONO DE LA EDITORIAL"
                },
                "sitio_web": {
                    required: "INGRESE EL SITIO WEB DE LA EDITORIAL"
                }
            }
        });
    });
</script>
