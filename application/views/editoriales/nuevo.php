
<div class="row">
<div class="col-md-4 ">

</div>
<div class="col-md-8 ">
  <br>
  <br>
<h1>
  <b>
  <i class="fa fa-plus-circle"></i>
  NUEVA EDITORIAL
  </b>
</h1>
<br>
<form action="<?php echo site_url('editoriales/guardarEditorial');?>" method="post" enctype="multipart/form-data" id="frm_nuevo_editorial" >
  <label for="nombre" class="error-message">nombre:</label>
  <input type="text" name="nombre" id="nombre" class="form-control" placeholder="Ingrese el nombre">
  <br>

  <label for="direccion" class="error-message">direccion:</label>
  <input type="text" name="direccion" id="direccion" class="form-control" placeholder="Ingrese la dirección">
  <br>

  <label for="contacto" class="error-message">contacto:</label>
  <input type="text" name="contacto" id="contacto" class="form-control" placeholder="Ingrese el teléfono">
  <br>

  <label for="sitio_web" class="error-message">SITIO WEB:</label>
  <input type="text" name="sitio_web" id="sitio_web" class="form-control" placeholder="Ingrese el sitio web">
  <br>



  </div>

</div>

<div class="row">
  <div class="col-md-12">
    <br>
    <div id="mapa" style="height:1px; width: 100%; border:opx solid black;"></div>
  </div>
</div>
<br>

    <div class="row">
      <div class="cold-md-12 text-center">
        <button type="submit" name="button" class="btn btn-primary"> <i class="fa-solid fa-floppy-disk"></i> GUARDAR</button>
        <a href="<?php echo site_url('editoriales/index');?>" class="btn btn-danger"> <i class="fa-solid fa-ban"></i>
          CANCELAR
        </a>
      </div>
    </div>


</form>
<br><br>






<script type="text/javascript">
    $(document).ready(function(){
        $("#frm_nuevo_editorial").validate({
            rules:{
                "nombre": {
                    required: true,
                    lettersonly: true,
                    minlength: 2,
                    maxlength: 50
                },
                "direccion": {
                    required: true,
                    minlength: 5,
                    maxlength: 100
                },
                "contacto": {
                    required: true
                },
                "sitio_web": {
                    required: true
                }
            },
            messages:{
                "nombre": {
                    required: "DEBE INGRESAR EL nombre DE LA EDITORIAL",
                    lettersonly: "DEBE CONTENER SOLO LETRAS",
                    minlength: "EL nombre DEBE TENER AL MENOS 2 CARACTERES",
                    maxlength: "EL nombre NO PUEDE TENER MÁS DE 50 CARACTERES"
                },
                "direccion": {
                    required: "DEBE INGRESAR LA DIRECCIÓN DE LA EDITORIAK",
                    minlength: "LA DIRECCIÓN DEBE TENER AL MENOS 5 CARACTERES",
                    maxlength: "LA DIRECCIÓN NO PUEDE TENER MÁS DE 100 CARACTERES"
                },
                "contacto": {
                    required: "DEBE INGRESAR EL TELÉFONO DE LA EDITORIAL"
                },
                "sitio_web": {
                    required: "INGRESE EL SITIO WEB DE LA EDITORIAL"
                }
            }
        });
    });
</script>
