<div class="row">
  <div class="col-md-4">
    <!-- Espacio reservado para otros elementos en la columna izquierda -->
  </div>
  <div class="col-md-8">
    <br>
    <br>
    <br>
    <br>
      <h1><i class="fa fa-edit"></i> EDITAR COAUTOR</h1>
      <form method="post" action="<?php echo site_url('coautores/actualizarCoautor'); ?>" enctype="multipart/form-data" id="formulario_coautor">
        <!-- Campo oculto para ID -->
        <input type="hidden" name="id_coa" id="id_coa" value="<?php echo $coautorEditar->id_coa; ?>">

        <!-- Campo para seleccionar autor -->
        <div class="form-group">
          <label for="id_au"><b>AUTOR PRINCIPAL:</b></label>
          <select name="id_au" id="id_au" class="form-control" required>
            <?php foreach ($autores as $autor) : ?>
              <option value="<?php echo $autor->id_au; ?>" <?php echo isset($coautorEditar) && $autor->id_au == $coautorEditar->id_au ? 'selected' : ''; ?>><?php echo $autor->nombres; ?></option>
            <?php endforeach; ?>
          </select>
        </div>

        <!-- Campos para información del coautor -->
        <div class="form-group">
          <label for="nombres"><b>NOMBRES:</b></label>
          <input type="text" name="nombres" id="nombres" value="<?php echo $coautorEditar->nombres; ?>" placeholder="Ingrese los nombres" class="form-control" required>
        </div>

        <div class="form-group">
          <label for="apellidos"><b>APELLIDOS:</b></label>
          <input type="text" name="apellidos" id="apellidos" value="<?php echo $coautorEditar->apellidos; ?>" placeholder="Ingrese los apellidos" class="form-control" required>
        </div>

        <div class="form-group">
          <label for="contribucion"><b>CONTRIBUCIÓN:</b></label>
          <input type="text" name="contribucion" id="contribucion" value="<?php echo $coautorEditar->contribucion; ?>" placeholder="Ingrese la contribución" class="form-control" required>
        </div>

        <!-- Botones para actualizar y cancelar -->
        <div class="row">
          <div class="col-md-12 text-center">
            <button type="submit" name="button" class="btn btn-primary">
              <i class="fa fa-pen"></i> &nbsp;ACTUALIZAR
            </button> &nbsp;&nbsp;
            <a href="<?php echo site_url('coautores/index'); ?>" class="btn btn-danger">
              <i class="fa fa-times"></i> &nbsp;CANCELAR
            </a>
          </div>
        </div>
      </form>
    </div>

</div>

<script type="text/javascript">
  // Crear la regla personalizada para solo letras y espacios
  $.validator.addMethod("lettersonly", function(value, element) {
    return this.optional(element) || /^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]+$/.test(value); // Solo letras y espacios
  }, "Solo se permiten letras y espacios");

  // Validar el formulario
  $("#formulario_coautor").validate({
    rules: {
      "nombres": {
        required: true,
        minlength: 2,
        maxlength: 100,
        lettersonly: true // Aplicar la regla personalizada
      },
      "apellidos": {
        required: true,
        minlength: 2,
        maxlength: 50,
        lettersonly: true
      },
      "contribucion": {
        required: true,
        minlength: 2,
        maxlength: 50,
        lettersonly: true
      }
    },
    messages: {
      "nombres": {
        required: "Por favor, ingrese los nombres",
        lettersonly: "Solo se permiten letras y espacios",
        minlength: "El nombre debe tener al menos 2 caracteres",
        maxlength: "El nombre no puede tener más de 100 caracteres"
      },
      "apellidos": {
        required: "Por favor, ingrese los apellidos",
        lettersonly: "Solo se permiten letras y espacios",
        minlength: "Los apellidos deben tener al menos 2 caracteres",
        maxlength: "Los apellidos no pueden tener más de 50 caracteres"
      },
      "contribucion": {
        required: "Por favor, ingrese la contribución",
        lettersonly: "Solo se permiten letras y espacios",
        minlength: "La contribución debe tener al menos 2 caracteres",
        maxlength: "La contribución no puede tener más de 50 caracteres"
      }
    },
    errorClass: "text-danger" // Clase para estilo de mensajes de error
  });
</script>
