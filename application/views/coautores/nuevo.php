<div class="row">


<div class="col-md-4 ">

</div>
<div class="col-md-8 ">
  <br>
  <br>
  <br>
  <br>
<div class="container">
  <h1>
    <b>
      <i class="fa fa-plus-circle"></i>
      NUEVO COAUTOR
    </b>
  </h1>
  <br>
  <div class="row">
    <div class="col-md-6">
      <form action="<?php echo site_url('coautores/guardarCoautor');?>" method="post" enctype="multipart/form-data" id="formulario_coautor">

        <div class="form-group">
          <label for="id_au"><b>AUTOR PRINCIPAL:</b></label>
          <select name="id_au" id="id_au" class="form-control" required>
              <?php foreach ($autores as $autor) : ?>
                  <option value="<?php echo $autor->id_au; ?>" <?php echo isset($coautorEditar) && $autor->id_au == $coautorEditar->id_au ? 'selected' : ''; ?>><?php echo $autor->nombres; ?></option>
              <?php endforeach; ?>
          </select>
        </div>

        <div class="form-group">
          <label for="nombres">NOMBRES:</label>
          <input type="text" name="nombres" id="nombres" class="form-control" placeholder="Ingrese el nombre del coautor">
        </div>

        <div class="form-group">
          <label for="apellidos">APELLIDOS:</label>
          <input type="text" name="apellidos" id="apellidos" class="form-control" placeholder="Ingrese los apellidos del coautor">
        </div>

        <div class="form-group">
          <label for="contribucion">CONTRIBUCIÓN:</label>
          <input type="text" name="contribucion" id="contribucion" class="form-control" placeholder="Ingrese el tipo de contribución">
        </div>

        <br>
        <div class="form-group">
          <button type="submit" name="button" class="btn btn-primary">
            <i class="fa-solid fa-floppy-disk"></i> GUARDAR
          </button>
          <a href="<?php echo site_url('coautores/index');?>" class="btn btn-danger">
            <i class="fa-solid fa-ban"></i> CANCELAR
          </a>
        </div>
      </form>
    </div>
  </div>
</div>
</div>
<script type="text/javascript">
// Crear la regla personalizada para solo letras y espacios
$.validator.addMethod("lettersonly", function(value, element) {
return this.optional(element) || /^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]+$/.test(value); // Solo letras y espacios
}, "Solo se permiten letras y espacios");

// Validar el formulario
$("#formulario_coautor").validate({
rules: {
"nombres": {
  required: true,
  minlength: 2,
  maxlength: 100,
  lettersonly: true // Aplicar la regla personalizada
},
"apellidos": {
  required: true,
  minlength: 2,
  maxlength: 50,
  lettersonly: true
},
"contribucion": {
  required: true,
  minlength: 2,
  maxlength: 50,
  lettersonly: true
}
},
messages: {
"nombres": {
  required: "Por favor, ingrese los nombres",
  lettersonly: "Solo se permiten letras y espacios",
  minlength: "El nombre debe tener al menos 2 caracteres",
  maxlength: "El nombre no puede tener más de 100 caracteres"
},
"apellidos": {
  required: "Por favor, ingrese los apellidos",
  lettersonly: "Solo se permiten letras y espacios",
  minlength: "Los apellidos deben tener al menos 2 caracteres",
  maxlength: "Los apellidos no pueden tener más de 50 caracteres"
},
"contribucion": {
  required: "Por favor, ingrese la contribución",
  lettersonly: "Solo se permiten letras y espacios",
  minlength: "La contribución debe tener al menos 2 caracteres",
  maxlength: "La contribución no puede tener más de 50 caracteres"
}
},
errorClass: "text-danger" // Clase para estilo de mensajes de error
});

</script>
