<div class="row">

<div class="col-md-4 ">

</div>
<div class="col-md-8 ">
  <br>
  <br>
  <br>
  <br>
<h1>
  <i class="fa fa-credit-card"></i>
  COAUTORES
</h1>

<div class="row">
  <div class="col-md-12 text-end">

    <!-- Button trigger modal -->
    <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
      <i class="fa fa-eye"></i> Ver mapa
    </button>

    <a href="<?php echo site_url('coautores/nuevo');?>" class="btn btn-outline-success">
      <i class="fa fa-plus-circle"></i>
      AGREGAR COAUTORES
    </a>
    <br><br>
  </div>
</div>

<?php if ($listadoCoautores): ?>
<table class="table table-bordered">
  <thead>
    <tr>
      <th>ID</th>
      <th>NOMBRES</th>
      <th>APELLIDOS</th>
      <th>CONTRIBUCION</th>
      <th>AUTOR</th>
      <th>ACCIONES</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($listadoCoautores as $coautor): ?>
    <tr>
      <td><?php echo $coautor->id_coa; ?></td>
      <td><?php echo $coautor->nombres; ?></td>
      <td><?php echo $coautor->apellidos; ?></td>
      <td><?php echo $coautor->contribucion; ?></td>
      <td>
        <?php
        if ($coautor->id_au) {
          $autor = $this->Autor->obtenerPorId($coautor->id_au);
          echo $autor->nombres;
        } else {
          echo 'N/A';
        }
        ?>
      </td>
      <td>
        <a href="<?php echo site_url('coautores/editar/') . $coautor->id_coa; ?>" class="btn btn-warning" title="Editar">
          <i class="fa fa-pen"></i>
          Editar
        </a>
        <a href="#" class="btn btn-danger eliminar-coautores" data-id="<?php echo $coautor->id_coa; ?>">
          <i class="fa fa-trash"></i>
          Eliminar
        </a>
      </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<!-- JavaScript para manejar la eliminación de coautores -->
<script>
  document.addEventListener('DOMContentLoaded', function () {
    const eliminarLinks = document.querySelectorAll('.eliminar-coautores'); // Corrección aquí

    eliminarLinks.forEach(link => {
      link.addEventListener('click', function (event) {
        event.preventDefault();
        const id_coa = this.getAttribute('data-id');
        const confirmar = confirm('¿Está seguro de que desea eliminar este coautor?');
        if (confirmar) {
          window.location.href = "<?php echo site_url('coautores/borrar/'); ?>" + id_coa;
        }
      });
    });
  });
</script>



<?php else: ?>
<div class="alert alert-danger">
  No se encontraron agencias de sucursal registradas
</div>
<?php endif; ?>
</div>
</div>
