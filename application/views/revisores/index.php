<div class="row">
<div class="col-md-4 ">

</div>
<div class="col-md-8 ">
  <br>
  <br>
  <br>
  <br>
<h1>
  <i class="fa fa-credit-card"></i>
  REVISORES
</h1>

<div class="row">
  <div class="col-md-12 text-end">

    <!-- Button trigger modal -->


    <a href="<?php echo site_url('revisores/nuevo');?>" class="btn btn-outline-success">
      <i class="fa fa-plus-circle"></i>
      AGREGAR REVISORES
    </a>
    <br><br>
  </div>
</div>

<?php if ($listadoRevisores): ?>
<table class="table table-bordered">
  <thead>
    <tr>
      <th>ID</th>
      <th>NOMBRES</th>
      <th>APELLIDOS</th>
      <th>ESPECIALIDAD</th>
      <th>ACCIONES</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($listadoRevisores as $revisor): ?>
    <tr>
      <td><?php echo $revisor->id_rev; ?></td>
      <td><?php echo $revisor->nombres; ?></td>
      <td><?php echo $revisor->apellidos; ?></td>
      <td><?php echo $revisor->area_espec; ?></td>

      <td>
        <a href="<?php echo site_url('revisores/editar/') . $revisor->id_rev; ?>" class="btn btn-warning" title="Editar">
          <i class="fa fa-pen"></i>
          Editar
        </a>
        <a href="#" class="btn btn-danger eliminar-revisores" data-id="<?php echo $revisor->id_rev; ?>">
          <i class="fa fa-trash"></i>
          Eliminar
        </a>
      </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<!-- JavaScript para manejar la eliminación de coautores -->
<script>
  document.addEventListener('DOMContentLoaded', function () {
    const eliminarLinks = document.querySelectorAll('.eliminar-revisores'); // Corrección aquí

    eliminarLinks.forEach(link => {
      link.addEventListener('click', function (event) {
        event.preventDefault();
        const id_rev = this.getAttribute('data-id');
        const confirmar = confirm('¿Está seguro de que desea eliminar este revisor?');
        if (confirmar) {
          window.location.href = "<?php echo site_url('revisores/borrar/'); ?>" + id_rev;
        }
      });
    });
  });
</script>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
</div>
<?php else: ?>
<div class="alert alert-danger">
  No se encontraron agencias de sucursal registradas
</div>
<?php endif; ?>
