<div class="row">
<div class="col-md-4 ">

</div>
<div class="col-md-8 ">
  <br>
  <br>
  <br>
  <br>
<div class="container">
  <h1><i class="fa fa-edit"></i> EDITAR REVISORES</h1>
  <form method="post" action="<?php echo site_url('revisores/actualizarRevisor'); ?>" enctype="multipart/form-data" id="formulario_coautor">
    <!-- Campo oculto para ID -->
    <input type="hidden" name="id_rev" id="id_rev" value="<?php echo $revisorEditar->id_rev; ?>">

    <!-- Campo para seleccionar autor -->


    <!-- Campos para información del coautor -->
    <div class="form-group">
      <label for="nombres"><b>NOMBRES:</b></label>
      <input type="text" name="nombres" id="nombres" value="<?php echo $revisorEditar->nombres; ?>" placeholder="Ingrese los nombres" class="form-control" required>
    </div>

    <div class="form-group">
      <label for="apellidos"><b>APELLIDOS:</b></label>
      <input type="text" name="apellidos" id="apellidos" value="<?php echo $revisorEditar->apellidos; ?>" placeholder="Ingrese los apellidos" class="form-control" required>
    </div>

    <div class="form-group">
      <label for="area_espec"><b>ESPECIALIDAD:</b></label>
      <input type="text" name="area_espec" id="area_espec" value="<?php echo $revisorEditar->area_espec; ?>" placeholder="Ingrese la especialidad" class="form-control" required>
    </div>

    <!-- Botones para actualizar y cancelar -->
    <div class="row">
      <div class="col-md-12 text-center">
        <button type="submit" name="button" class="btn btn-primary">
          <i class="fa fa-pen"></i> &nbsp;ACTUALIZAR
        </button> &nbsp;&nbsp;
        <a href="<?php echo site_url('revisores/index'); ?>" class="btn btn-danger">
          <i class="fa fa-times"></i> &nbsp;CANCELAR
        </a>
      </div>
    </div>
  </form>
</div>
<script type="text/javascript">
// Crear la regla personalizada para solo letras y espacios
$.validator.addMethod("lettersonly", function(value, element) {
return this.optional(element) || /^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]+$/.test(value); // Solo letras y espacios
}, "Solo se permiten letras y espacios");

// Validar el formulario
$("#formulario_coautor").validate({
rules: {
"nombres": {
  required: true,
  minlength: 2,
  maxlength: 100,
  lettersonly: true // Aplicar la regla personalizada
},
"apellidos": {
  required: true,
  minlength: 2,
  maxlength: 50,
  lettersonly: true
},
"area_espec": {
  required: true,
  minlength: 2,
  maxlength: 50,
  lettersonly: true
}
},
messages: {
"nombres": {
  required: "Por favor, ingrese los nombres",
  lettersonly: "Solo se permiten letras y espacios",
  minlength: "El nombre debe tener al menos 2 caracteres",
  maxlength: "El nombre no puede tener más de 100 caracteres"
},
"apellidos": {
  required: "Por favor, ingrese los apellidos",
  lettersonly: "Solo se permiten letras y espacios",
  minlength: "Los apellidos deben tener al menos 2 caracteres",
  maxlength: "Los apellidos no pueden tener más de 50 caracteres"
},
"area_espec": {
  required: "Por favor, ingrese la especialidad",
  lettersonly: "Solo se permiten letras y espacios",
  minlength: "La contribución debe tener al menos 2 caracteres",
  maxlength: "La contribución no puede tener más de 50 caracteres"
}
},
errorClass: "text-danger" // Clase para estilo de mensajes de error
});

</script>
