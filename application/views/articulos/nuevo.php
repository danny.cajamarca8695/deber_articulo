<div class="row">
<div class="col-md-4 ">

</div>
<div class="col-md-8 ">
  <br>

  <form class="" method="post" action="<?php echo site_url('articulos/guardar'); ?>" enctype="multipart/form-data">
    <h1>Nuevo articulo</h1>
    <div class="form-group">
            <label for="nombre">Nombre:</label>
            <input type="text" class="form-control" id="nombre" name="nombre" required>
        </div>
        <div class="form-group">
            <label for="resumen">Resumen:</label>
            <textarea class="form-control" id="resumen" name="resumen" rows="3" required></textarea>
        </div>
        <div class="form-group">
            <label for="fecha_publi">Fecha de Publicación:</label>
            <input type="date" class="form-control" id="fecha_publi" name="fecha_publi" required>
        </div>
        <div class="form-group">
            <label for="pdf">PDF:</label>
            <input type="file" class="form-control-file" id="pdf" name="pdf" required>
        </div>
        <div class="form-group">
            <label for="id_edi">ID de Editorial:</label>
            <select class="form-control" id="id_edi" name="id_edi" required>
                <option value="">Seleccione un editor</option>
                <?php foreach ($listadoEditor as $tipo): ?>
                  <option value="<?php echo $tipo->id_edi; ?>"><?php echo $tipo->nombre; ?></option>
              <?php endforeach; ?>
                <!-- Agrega más opciones según sea necesario -->
            </select>
        </div>
        <div class="form-group">
            <label for="id_rev">ID de Revisor:</label>
            <select class="form-control" id="id_rev" name="id_rev" required>
                <option value="">Seleccione un Revisor</option>
                <?php foreach ($listadoRevisor as $tipo): ?>
                  <option value="<?php echo $tipo->id_rev; ?>"><?php echo $tipo->nombre; ?></option>
              <?php endforeach; ?>
                <!-- Agrega más opciones según sea necesario -->
            </select>
        </div>
        <div class="form-group">
    <label for="id_tip">ID de Tipo:</label>
    <select class="form-control" id="id_tip" name="id_tip" required>
        <option value="">Seleccione un tipo</option>
        <?php foreach ($listadoTipo as $tipo): ?>
          <option value="<?php echo $tipo->id; ?>"><?php echo $tipo->tipo; ?></option>
      <?php endforeach; ?>
        <!-- Agrega más opciones según sea necesario -->
    </select>
</div>

    <center>
    <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-floppy-disk fa-bounce"></i> &nbsp Guardar</button> &nbsp &nbsp
    <a href="<?php echo site_url('hospitales/index'); ?>" class="btn btn-danger"> <i class="fa fa-xmark fa-spin"></i> &nbsp Cancelar</a>
  </center>
  </form>

</div>
</div>
