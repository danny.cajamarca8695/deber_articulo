<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-8">
        <br>
        <br>
        <a href="<?php echo site_url('articulos/nuevo')?>" class="btn btn-secondary">NUEVO</a>
        <br>
        <br>
        <?php if ($listado): ?>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <td>ID</td>
                        <td>NOMBRE</td>
                        <td>RESUMEN</td>
                        <td>FECHA PUBLICACION</td>
                        <td>PDF</td>
                        <td>EDITOR</td>
                        <td>REVISOR</td>
                        <td>TIPO</td>
                        <td>ACCIONES</td>
                        <!-- Nueva columna para el botón de generación de PDF -->
                        <td>GENERAR PDF</td>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($listado as $articulo): ?>
                        <tr>
                            <!-- Datos del artículo -->
                            <td><?php echo $articulo->id_art; ?></td>
                            <td><?php echo $articulo->nombre; ?></td>
                            <td><?php echo $articulo->resumen; ?></td>
                            <td><?php echo $articulo->fecha_publi; ?></td>
                            <td>
                                <?php if ($articulo->pdf): ?>
                                    <a href="<?php echo base_url('uploads/articulos/' . $articulo->pdf); ?>" target="_blank">Ver PDF</a>
                                <?php else: ?>
                                    No disponible
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php
                                foreach ($listadoEditor as $tipo) {
                                    if ($tipo->id_edi == $articulo->id_edi) {
                                        echo $tipo->nombre;
                                        break; // Salir del bucle una vez que se encuentra el tipo correspondiente
                                    }
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                foreach ($listadoRevisor as $tipo) {
                                    if ($tipo->id_rev == $articulo->id_rev) {
                                        echo $tipo->nombre;
                                        break; // Salir del bucle una vez que se encuentra el tipo correspondiente
                                    }
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                foreach ($listadoTipo as $tipo) {
                                    if ($tipo->id == $articulo->id_tip) {
                                        echo $tipo->tipo;
                                        break; // Salir del bucle una vez que se encuentra el tipo correspondiente
                                    }
                                }
                                ?>
                            </td>
                            <!-- Botones de editar y eliminar -->
                            <td>
                                <a href="<?php echo site_url('articulos/editar/') . $articulo->id_art; ?>" class="btn btn-warning">EDITAR</a>
                                <a href="<?php echo site_url('articulos/borrar/') . $articulo->id_art; ?>" class="btn btn-danger"> ELIMINAR</a>
                            </td>
                            <!-- Botón para generar PDF -->
                            <td>
                                <a href="<?php echo site_url('articulos/generar_pdf/') . $articulo->id_art; ?>" class="btn btn-primary">GENERAR PDF</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        <?php else: ?>
            <div class="alert alert-danger">
                No se encontraron registros en la tabla hospitales
            </div>
        <?php endif; ?>
    </div>
</div>
