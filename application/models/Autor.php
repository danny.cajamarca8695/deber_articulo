<?php
  class Autor extends CI_MODEL
  {

    function __construct()
    {
      parent::__construct();
    }
    //insertar nuevos hospitales
    function insertar($datos){
      $respuesta=$this->db->insert("autor",$datos);
      return $respuesta;
    }
    //consulta de datos

    function consultarTodos(){
      $autores=$this->db->get("autor");
      if ($autores->num_rows()>0){
        return $autores->result();
      } else {
        return false;
      }
    }

    //eliminacion de hopsital por id
    function eliminar($id){
      $this->db->where("id_au",$id);
      return $this->db->delete("autor");
    }

    //Consulta de un solo hospital
    function obtenerPorId($id){
      $this->db->where("id_au",$id);
      $autor=$this->db->get("autor");
      if ($autor->num_rows()>0) {
        return $autor->row();
      } else {
        return false;
      }
    }

    //FUNCION PARA ACTUALIZAR HOSPITALES
    function actualizar($id,$datos){
      $this->db->where("id_au",$id);
      return $this->db->update("autor",$datos);
    }

    //FUNCION PARA OBTENER NOMBRE DE LA AGENCIA POR ID
    public function obtenerNombrePorId($id_au) {
    // Consultar la base de datos para obtener el nombre de la agencia por su ID
    $query = $this->db->get_where('autor', array('id_au' => $id_au));

    // Verificar si se encontró alguna agencia con el ID proporcionado
    if ($query->num_rows() > 0) {
        // Obtener el resultado de la consulta y devolver el nombre de la agencia
        $autor = $query->row();
        return $autor->nombres; // Asumiendo que el campo se llama nombre_ba
    } else {
        // Si no se encuentra ninguna agencia, devolver un valor predeterminado o lanzar una excepción
        return 'AUTOR NO ASIGNADA';
    }
}




  } //fin de la clase

 ?>
