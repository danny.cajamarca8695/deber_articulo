<?php
  class Editor extends CI_MODEL
  {

    function __construct()
    {
      parent::__construct();
    }
    //insertar nuevos hospitales
    function insertar($datos){
      $respuesta=$this->db->insert("editor",$datos);
      return $respuesta;
    }
    //consulta de datos

    function consultarTodos(){
      $editores=$this->db->get("editor");
      if ($editores->num_rows()>0){
        return $editores->result();
      } else {
        return false;
      }
    }

    //eliminacion de hopsital por id
    function eliminar($id){
      $this->db->where("id_ed",$id);
      return $this->db->delete("editor");
    }

    //Consulta de un solo hospital
    function obtenerPorId($id){
      $this->db->where("id_ed",$id);
      $editor=$this->db->get("editor");
      if ($editor->num_rows()>0) {
        return $editor->row();
      } else {
        return false;
      }
    }

    function actualizar($id, $datos){
    $this->db->where("id_ed", $id);
    $this->db->set($datos);
    return $this->db->update("editor"); // Solo el nombre de la tabla sin pasar $datos nuevamente
    } 

    //FUNCION PARA OBTENER NOMBRE DE LA AGENCIA POR ID
    public function obtenerNombrePorId($id_ed) {
    // Consultar la base de datos para obtener el nombre de la agencia por su ID
    $query = $this->db->get_where('editor', array('id_ed' => $id_ed));

    // Verificar si se encontró alguna agencia con el ID proporcionado
    if ($query->num_rows() > 0) {
        // Obtener el resultado de la consulta y devolver el nombre de la agencia
        $editor = $query->row();
        return $editor->nombre; // Asumiendo que el campo se llama nombre_ba
    } else {
        // Si no se encuentra ninguna agencia, devolver un valor predeterminado o lanzar una excepción
        return 'EDITOR NO ASIGNADA';
    }
}

 // OBTENER ARTICULO POR ID
    function obtenerPorIdArticulo($id_art)
    {
        $this->db->where("id_art", $id_art);
        $editores = $this->db->get("editores");
        if ($editores->num_rows() > 0) {
            return $editores->result();
        } else {
            return false;
        }
    }




  } //fin de la clase

 ?>
