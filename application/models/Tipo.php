<?php
  class Tipo extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Insertar nuevos hospitales
    function insertar($datos){
      $respuesta=$this->db->insert("tipo",$datos);
      return $respuesta;
    }
    //Consulta de datos
    function consultarTodos(){
      $tipos=$this->db->get("tipo");
      if ($tipos->num_rows()>0) {
        return $tipos->result();
      } else {
        return false;
      }
    }
    //eliminacion de hospital por id
    function eliminar($id){
        $this->db->where("id",$id);
        return $this->db->delete("tipo");
    }
      //Consulta de un solo hospital
      //Consulta de un solo hospital
    function obtenerPorId($id){
      $this->db->where("id",$id);
      $tipo=$this->db->get("tipo");
      if ($tipo->num_rows()>0) {
        return $tipo->row();
      } else {
        return false;
      }
    }
    // funcion para actualizar hospitale
    function actualizar($id,$datos){
      $this->db->where("id",$id);
      return $this->db->update("tipo",$datos);
    }

  }//Fin de la clase
?>
