<?php
  class Articulo extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Insertar nuevos hospitales
    function insertar($datos){
      $respuesta=$this->db->insert("articulos",$datos);
      return $respuesta;
    }
    //Consulta de datos
    function consultarTodos(){
      $articulos=$this->db->get("articulos");
      if ($articulos->num_rows()>0) {
        return $articulos->result();
      } else {
        return false;
      }
    }
    //eliminacion de hospital por id
    function eliminar($id){
        $this->db->where("id_art",$id);
        return $this->db->delete("articulos");
    }
      //Consulta de un solo hospital
      //Consulta de un solo hospital
    function obtenerPorId($id){
      $this->db->where("id_art",$id);
      $tipo=$this->db->get("articulos");
      if ($tipo->num_rows()>0) {
        return $tipo->row();
      } else {
        return false;
      }
    }
    // funcion para actualizar hospitale
    function actualizar($id,$datos){
      $this->db->where("id_art",$id);
      return $this->db->update("articulos",$datos);
    }
    public function obtenerDatosParaPDF($id_articulo) {
        $this->db->select('a.nombre AS nombre_articulo, a.resumen, a.fecha_publi, a.pdf, e.nombre AS nombre_editorial, r.nombre AS nombre_revista, GROUP_CONCAT(au.nombres, " ", au.apellidos) AS autores');
        $this->db->from('articulos AS a');
        $this->db->join('editorial AS e', 'a.id_edi = e.id_edi', 'left');
        $this->db->join('revista AS r', 'a.id_rev = r.id_rev', 'left');
        $this->db->join('art_aut AS aa', 'a.id_art = aa.id_art', 'left');
        $this->db->join('autor AS au', 'aa.id_aut = au.id_au', 'left');
        $this->db->where('a.id_art', $id_articulo);
        $query = $this->db->get();
        return $query->row();
    }
  }//Fin de la clase
?>
