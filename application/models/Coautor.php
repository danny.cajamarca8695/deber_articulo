<?php
class Coautor extends CI_MODEL
{

  function __construct()
  {
    parent::__construct();
  }

  // Insertar nuevo cajero
  function insertar($datos){
    $respuesta = $this->db->insert("coautor", $datos);
    return $respuesta;
  }

  // Consultar todos los cajeros

  function consultarTodos()
{
    $coautores = $this->db->get("coautor");
    if ($coautores->num_rows() > 0) {
        return $coautores->result();
    } else {
        return false;
    }
}

  // Eliminar cajero por id
  function eliminar($id){
    $this->db->where("id_coa", $id);
    return $this->db->delete("coautor");
  }

  // Obtener cajero por id
  function obtenerPorId($id){
    $this->db->where("id_coa", $id);
    $query = $this->db->get("coautor");

    if ($query->num_rows() > 0) {
      return $query->row();
    } else {
      return false;
    }
  }

  // Actualizar cajero
  function actualizar($id, $datos){
    $this->db->where("id_coa", $id);
    return $this->db->update("coautor", $datos);
  }
  function obtenerPorIdAutor($id_au)
    {
        $this->db->where("id_au", $id_au);
        $coautores = $this->db->get("coautor");
        if ($coautores->num_rows() > 0) {
            return $coautores->result();
        } else {
            return false;
        }
    }

} // Fin de la clase
?>
