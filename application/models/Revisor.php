<?php
class Revisor extends CI_MODEL
{

  function __construct()
  {
    parent::__construct();
  }

  // Insertar nuevo cajero
  function insertar($datos){
    $respuesta = $this->db->insert("revisor", $datos);
    return $respuesta;
  }

  // Consultar todos los cajeros

  function consultarTodos()
{
    $revisores = $this->db->get("revisor");
    if ($revisores->num_rows() > 0) {
        return $revisores->result();
    } else {
        return false;
    }
}

  // Eliminar cajero por id
  function eliminar($id){
    $this->db->where("id_rev", $id);
    return $this->db->delete("revisor");
  }

  // Obtener cajero por id
  function obtenerPorId($id){
    $this->db->where("id_rev", $id);
    $query = $this->db->get("revisor");

    if ($query->num_rows() > 0) {
      return $query->row();
    } else {
      return false;
    }
  }

  // Actualizar cajero
  function actualizar($id, $datos){
    $this->db->where("id_rev", $id);
    return $this->db->update("revisor", $datos);
  }


} // Fin de la clase
?>
