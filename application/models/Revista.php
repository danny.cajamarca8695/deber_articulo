<?php
  class Revista extends CI_MODEL
  {

    function __construct()
    {
      parent::__construct();
    }
    //insertar nuevos editoriales
    function insertar($datos){
      $respuesta=$this->db->insert("revista",$datos);
      return $respuesta;
    }
    //consulta de datos
    function consultarTodos(){
      $revistas=$this->db->get("revista");
      if ($revistas->num_rows()>0){
        return $revistas->result();
      } else {
        return false;
      }
    }

    //eliminacion de hopsital por id
    function eliminar($id){
      $this->db->where("id_rev",$id);
      return $this->db->delete("revista");
    }
    //Consulta de un solo editorial
        function obtenerPorId($id){
          $this->db->where("id_rev",$id);
          $revista=$this->db->get("revista");
          if ($revista->num_rows()>0) {
            return $revista->row();
          } else {
            return false;
          }
        }


        //funcion para actualizar editoriales
          function actualizar($id,$datos){
            $this->db->where("id_rev",$id);
            return $this->db
                        ->update("revista",$datos);
          }

  } //fin de la clase

 ?>
