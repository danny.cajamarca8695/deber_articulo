<?php
  class Editorial extends CI_MODEL
  {

    function __construct()
    {
      parent::__construct();
    }
    //insertar nuevos editoriales
    function insertar($datos){
      $respuesta=$this->db->insert("editorial",$datos);
      return $respuesta;
    }
    //consulta de datos
    function consultarTodos(){
      $editoriales=$this->db->get("editorial");
      if ($editoriales->num_rows()>0){
        return $editoriales->result();
      } else {
        return false;
      }
    }

    //eliminacion de hopsital por id
    function eliminar($id){
      $this->db->where("id_edi",$id);
      return $this->db->delete("editorial");
    }
    //Consulta de un solo editorial
        function obtenerPorId($id){
          $this->db->where("id_edi",$id);
          $editorial=$this->db->get("editorial");
          if ($editorial->num_rows()>0) {
            return $editorial->row();
          } else {
            return false;
          }
        }


        //funcion para actualizar editoriales
          function actualizar($id,$datos){
            $this->db->where("id_edi",$id);
            return $this->db
                        ->update("editorial",$datos);
          }

  } //fin de la clase

 ?>
