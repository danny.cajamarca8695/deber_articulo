<?php

  class Editoriales extends CI_Controller
  {

    function __construct()
    {
      parent::__construct();
      $this->load->model("Editorial");

      //desabilitando errores y advertencias de php
      error_reporting(0);
    }
//Renderizacion de la vista index de hospitales
  public function index(){

    $data["listadoEditoriales"]=$this->Editorial->consultarTodos();
    $this->load->view("header");
    $this->load->view("editoriales/index",$data);
    $this->load->view("footer");
    }

    //ELIMINACION DE HOSPITALES RECIBINEDO EL ID POR get
    public function borrar($id_edi){
      $this->Editorial->eliminar($id_edi);
      $this->session->set_flashdata("confirmacion","Editorial eliminado correctamente");

      redirect("editoriales/index");
    }
    //renderizacion del formulario del nuevo hospitales
    public function nuevo(){
      $this->load->view("header");
      $this->load->view("editoriales/nuevo");
      $this->load->view("footer");
    }
    //capturando datos e insterdando  en hospitales
    public function guardarEditorial(){



      $datosNuevoEditorial=array(
        "nombre"=>$this->input->post("nombre"),
        "direccion"=>$this->input->post("direccion"),
        "contacto"=>$this->input->post("contacto"),
        "sitio_web"=>$this->input->post("sitio_web"),

      );


      //set_flashdata sirve para crear una session de tipo flash
      $this->Editorial->insertar($datosNuevoEditorial);
      $this->session->set_flashdata("confirmacion", "Editorial guardado exitosamente");
      redirect('editoriales/index');

    }



    public function editar($id_edi){
            $data["editorialEditar"]=$this->Editorial->obtenerPorId($id_edi);
            $this->load->view("header");
            $this->load->view("editoriales/editar",$data);
            $this->load->view("footer");
          }

    public function actualizarEditorial() {
          $id_edi = $this->input->post("id_edi");

          $datosEditorial = array(
              "nombre" => $this->input->post("nombre"),
              "direccion" => $this->input->post("direccion"),
              "contacto" => $this->input->post("contacto"),
              "sitio_web" => $this->input->post("sitio_web")
          );

          $this->Editorial->actualizar($id_edi, $datosEditorial);

          $this->session->set_flashdata("confirmacion", "Editorial actualizado exitosamente");

          redirect('editoriales/index');
      }


    }//cierre de la clase

    ?>
