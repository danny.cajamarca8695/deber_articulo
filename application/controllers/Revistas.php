<?php

  class Revistas extends CI_Controller
  {

    function __construct()
    {
      parent::__construct();
      $this->load->model("Revista");

      //desabilitando errores y advertencias de php
      error_reporting(0);
    }
//Renderizacion de la vista index de hospitales
  public function index(){

    $data["listadoRevistas"]=$this->Revista->consultarTodos();
    $this->load->view("header");
    $this->load->view("revistas/index",$data);
    $this->load->view("footer");
    }

    //ELIMINACION DE HOSPITALES RECIBINEDO EL ID POR get
    public function borrar($id_rev){
      $this->Revista->eliminar($id_rev);
      $this->session->set_flashdata("confirmacion","Revista eliminado correctamente");

      redirect("revistas/index");
    }
    //renderizacion del formulario del nuevo hospitales
    public function nuevo(){
      $this->load->view("header");
      $this->load->view("revistas/nuevo");
      $this->load->view("footer");
    }
    //capturando datos e insterdando  en hospitales
    public function guardarRevista(){



      $datosNuevoRevista=array(
        "nombre"=>$this->input->post("nombre"),
        "issn"=>$this->input->post("issn"),
        "factor_impacto"=>$this->input->post("factor_impacto"),

      );


      //set_flashdata sirve para crear una session de tipo flash
      $this->Revista->insertar($datosNuevoRevista);
      $this->session->set_flashdata("confirmacion", "Revista guardado exitosamente");

    }



    public function editar($id_rev){
            $data["revistaEditar"]=$this->Revista->obtenerPorId($id_rev);
            $this->load->view("header");
            $this->load->view("revistas/editar",$data);
            $this->load->view("footer");
          }

    public function actualizarRevista() {
          $id_rev = $this->input->post("id_rev");

          $datosRevista = array(
              "nombre" => $this->input->post("nombre"),
              "issn" => $this->input->post("issn"),
              "factor_impacto" => $this->input->post("factor_impacto"),
          );

          $this->Revista->actualizar($id_rev, $datosRevista);

          $this->session->set_flashdata("confirmacion", "Revista actualizado exitosamente");

          redirect('revistas/index');
      }


    }//cierre de la clase

    ?>
