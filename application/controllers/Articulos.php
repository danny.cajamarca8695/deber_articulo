<?php
class Articulos extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Articulo");
        $this->load->model("Tipo");
        $this->load->model("Editorial");
        $this->load->model("Revista");
    }

    public function index(){
        $data["listado"] = $this->Articulo->consultarTodos();
        $data["listadoTipo"] = $this->Tipo->consultarTodos();
        $data["listadoEditor"] = $this->Editorial->consultarTodos();
        $data["listadoRevisor"] = $this->Revista->consultarTodos();
        $this->load->view("header");
        $this->load->view("articulos/index", $data);
    }

    public function borrar($id_art){
        $this->Articulo->eliminar($id_art);
        $this->session->set_flashdata("confirmacion","Tipo eliminado");
        redirect("articulos/index");
    }

    public function nuevo(){
        $data["listadoTipo"] = $this->Tipo->consultarTodos();
        $data["listadoEditor"] = $this->Editorial->consultarTodos();
        $data["listadoRevisor"] = $this->Revista->consultarTodos();
        $this->load->view("header");
        $this->load->view("articulos/nuevo",$data);
    }

    public function guardar() {
        /* INICIO PROCESO DE SUBIDA DE ARCHIVO */
        $config['upload_path'] = APPPATH . '../uploads/articulos/';
        $config['allowed_types'] = 'pdf';
        $config['max_size'] = 5 * 1024; // 5MB
        $nombre_aleatorio = "articulo_" . time() * rand(100, 10000);
        $config['file_name'] = $nombre_aleatorio;
        $this->load->library('upload', $config);

        if ($this->upload->do_upload("pdf")) {
            $dataArchivoSubido = $this->upload->data();
            $nombre_archivo_subido = $dataArchivoSubido["file_name"];
        } else {
            $nombre_archivo_subido = "";
        }

        // Obtener otros datos del formulario
        $datosNuevoArticulo = array(
            "nombre" => $this->input->post("nombre"),
            "resumen" => $this->input->post("resumen"),
            "fecha_publi" => $this->input->post("fecha_publi"),
            "pdf" => $nombre_archivo_subido,
            "id_edi" => $this->input->post("id_edi"),
            "id_rev" => $this->input->post("id_rev"),
            "id_tip" => $this->input->post("id_tip")
        );

        // Guardar en la base de datos
        $this->Articulo->insertar($datosNuevoArticulo);
        $this->session->set_flashdata("confirmacion", "Artículo guardado exitosamente");
        redirect('articulos/index');
    }

    public function editar($id){
        $data["editar"]=$this->Articulo->obtenerPorId($id);
        $data["listadoTipo"] = $this->Tipo->consultarTodos();
        $data["listadoEditor"] = $this->Editorial->consultarTodos();
        $data["listadoRevisor"] = $this->Revista->consultarTodos();
        $this->load->view("header");
        $this->load->view("articulos/editar",$data);
    }

    public function actualizar()
    {
        $id = $this->input->post("id");
        $datos = array(
            "nombre" => $this->input->post("nombre"),
            "resumen" => $this->input->post("resumen"),
            "fecha_publi" => $this->input->post("fecha_publi"),
            "id_edi" => $this->input->post("id_edi"),
            "id_rev" => $this->input->post("id_rev"),
            "id_tip" => $this->input->post("id_tip")
        );

        // Verificar si se subió un nuevo archivo PDF
        if (!empty($_FILES['pdf']['name'])) {
            $config['upload_path'] = APPPATH . '../uploads/articulos/';
            $config['allowed_types'] = 'pdf';
            $config['max_size'] = 5 * 1024; // 5MB
            $nombre_aleatorio = "articulo_" . time() * rand(100, 10000);
            $config['file_name'] = $nombre_aleatorio;
            $this->load->library('upload', $config);

            if ($this->upload->do_upload("pdf")) {
                $dataArchivoSubido = $this->upload->data();
                $nombre_archivo_subido = $dataArchivoSubido["file_name"];
                // Actualizar el nombre del PDF en los datos
                $datos["pdf"] = $nombre_archivo_subido;
            } else {
                // Si falla la carga del archivo, puedes manejar el error aquí
                // Por ejemplo, puedes mostrar un mensaje de error al usuario
                $error = array('error' => $this->upload->display_errors());
                print_r($error);
                exit();
            }
        }

        // Llamar al método de actualización del modelo
        $this->Articulo->actualizar($id, $datos);
        $this->session->set_flashdata("confirmacion", "Artículo actualizado exitosamente");
        redirect('articulos/index');
    }
    public function generar_pdf($id_articulo) {
        // Obtener datos del modelo
        $data['articulo'] = $this->Articulo->obtenerDatosParaPDF($id_articulo);

        // Construir la carta de aceptación
        $carta = ""; // Inicializa la variable $carta como una cadena vacía
        $carta .= "\n";
        $carta .= "\n";
        $carta .= "\n";
        $carta .= "\n";
        $carta .= "\n";
        $carta .= "\n";
        // Concatena cada línea de la carta
        $carta .= "{$data['articulo']->nombre_articulo}\n";
        $carta .= "\n";
        $carta .= "Fecha de publicación: {$data['articulo']->fecha_publi}\n";
        $carta .= "\n";
        $carta .= "Nombre de la revista: {$data['articulo']->nombre_revista}\n";
        $carta .= "\n";
        $carta .= "Nombre de la editorial: {$data['articulo']->nombre_editorial}\n\n";
        $carta .= "\n";
        $carta .= "\n";

        $carta .= "Estimado/a {$data['articulo']->autores},\n\n";
        $carta .= "\n";
        $carta .= "Espero que este mensaje te encuentre bien. Nos complace informarte que tu artículo titulado \"{$data['articulo']->nombre_articulo}\" ha sido aceptado para su publicación en {$data['articulo']->nombre_revista}.\n\n";
        $carta .= "\n";
        $carta .= "Felicitaciones por este logro. Tu trabajo ha pasado por un riguroso proceso de revisión y ha sido seleccionado debido a su calidad, relevancia y contribución al campo de estudio.\n\n";
        $carta .= "\n";
        $carta .= "Queremos agradecerte por tu compromiso y dedicación en la creación de este artículo. Creemos que tu investigación será valiosa para nuestra audiencia y esperamos que esta publicación contribuya significativamente a la comunidad académica.\n\n";
        $carta .= "\n";
        $carta .= "Adjunto a este correo, encontrarás las instrucciones para la preparación final de tu artículo antes de la publicación. Por favor, asegúrate de seguir estas instrucciones cuidadosamente y enviar los documentos requeridos dentro del plazo indicado.\n\n";
        $carta .= "\n";
        $carta .= "Una vez más, felicitaciones por esta realización y esperamos con interés trabajar contigo en la publicación de tu artículo.\n\n";
        $carta .= "\n";
        $carta .= "\n";
        $carta .= "\n";
        $carta .= "\n";
        $carta .= "Atentamente,\n";
        $carta .= "\n";
        $carta .= "\n";
        $carta .= "{$data['articulo']->nombre_revista}\n";

        // Crear instancia de TCPDF
        require_once(APPPATH . '/libraries/tcpdf/tcpdf.php');
        $pdf = new TCPDF();

        // Agregar contenido al PDF
        $pdf->AddPage();
        $pdf->SetFont('helvetica', '', 12);
        $pdf->Write(0, $carta);

        // Guardar PDF en el servidor o enviarlo como respuesta al navegador
        $pdf->Output('nombre_archivo.pdf', 'D');
    }



}
?>
