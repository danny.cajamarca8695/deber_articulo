<?php
class Coautores extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Coautor");
        $this->load->model("Autor");


        // Habilitar el reporte de errores
        error_reporting();
    }

    public function index()
    {
        $data["listadoCoautores"] = $this->Coautor->consultarTodos();
        $datos["listadoAutores"] = $this->Autor->consultarTodos();
        $this->load->view("header");
        $this->load->view("coautores/index", $data);
        $this->load->view("footer");
    }

    public function borrar($id_coa)
    {
        $this->Coautor->eliminar($id_coa);
        $this->session->set_flashdata("confirmacion", "Coautor eliminado correctamente");
        redirect("coautores/index");
    }

    public function nuevo()
    {
        $data["autores"] = $this->Autor->consultarTodos();
        $this->load->view("header");
        $this->load->view("coautores/nuevo", $data);
        $this->load->view("footer");
    }

    public function guardarCoautor()
    {
        // Proceso de subida de archivo (se puede agregar validación de archivo aquí)

        $datosNuevoCoautor = array(
            "id_au" => $this->input->post("id_au"),
            "nombres" => $this->input->post("nombres"),
            "apellidos" => $this->input->post("apellidos"),
            "contribucion" => $this->input->post("contribucion")
        );

        $this->Coautor->insertar($datosNuevoCoautor);
        $this->session->set_flashdata("confirmacion", "Coautor guardado exitosamente");
        redirect('coautores/index');
    }

    public function editar($id_coa)
    {
        $data["coautorEditar"] = $this->Coautor->obtenerPorId($id_coa);
        $data["autores"] = $this->Autor->consultarTodos();
        $this->load->view("header");
        $this->load->view("coautores/editar", $data);
        $this->load->view("footer");
    }

    public function actualizarCoautor()
    {
        $id_coa = $this->input->post("id_coa");

        // Proceso de subida de archivo (se puede agregar validación de archivo aquí)

        $datosCoautor = array(
            "id_au" => $this->input->post("id_au"),

            "nombres" => $this->input->post("nombres"),
            "apellidos" => $this->input->post("apellidos"),
            "contribucion" => $this->input->post("contribucion")
        );

        $this->Coautor->actualizar($id_coa, $datosCoautor);
        $this->session->set_flashdata("confirmacion", "Coautor actualizado exitosamente");
        redirect('coautores/index');
    }
}
?>
