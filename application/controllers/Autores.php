<?php

  class Autores extends CI_Controller
  {
      public function __construct()
      {
          parent::__construct();
          $this->load->model("Autor");

      }

      public function index()
      {
          $data["listadoAutores"] = $this->Autor->consultarTodos();
          $this->load->view("header");
          $this->load->view("autores/index", $data);

      }

      public function borrar($id_au=null)
      {
          if ($id_au) {
              $this->Autor->eliminar($id_au);
              $this->session->set_flashdata("confirmacion", "Autor eliminado correctamente");
          } else {
              // Manejar el caso cuando no se proporciona el ID del autor
              $this->session->set_flashdata("error", "Se requiere un ID de autor para eliminar");
          }
          redirect("autores/index");
      }

      public function nuevo()
      {
          # LLAMAR LOS AUTORES AGREGADAS
          $data["autores"] = $this->Autor->consultarTodos();
          $this->load->view("header");
          $this->load->view("autores/nuevo", $data);

      }

      public function guardarAutor()
      {
          $datosNuevoAutor = array(
              "nombres" => $this->input->post("nombres"),
              "apellidos" => $this->input->post("apellidos"),
              "afiliacion" => $this->input->post("afiliacion"),
              "correo" => $this->input->post("correo"),

          );
          $this->Autor->insertar($datosNuevoAutor);
          $this->session->set_flashdata("confirmacion", "Autor guardado exitosamente");
          redirect("autores/index");


      }

      public function editar($id)
      {
          $data["autorEditar"] = $this->Autor->obtenerPorId($id);
          $this->load->view("header");
          $this->load->view("autores/editar", $data);

      }

      public function actualizarAutor()
      {
          $id_au = $this->input->post("id_au");
          $datosAutor = array(
              "nombres" => $this->input->post("nombres"),
              "apellidos" => $this->input->post("apellidos"),
              "afiliacion" => $this->input->post("afiliacion"),
              "correo" => $this->input->post("correo"),
          );
          $this->Autor->actualizar($id_au, $datosAutor);
          $this->session->set_flashdata("confirmacion", "Autor actualizado exitosamente");
          redirect('autores/index');
      }

  }

?>
