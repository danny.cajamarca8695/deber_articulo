<?php
  class Tipos extends CI_Controller
  {
    function __construct()
    {
      parent::__construct();
      $this->load->model("Tipo");
    }
    //Renderizacion de la vista index de HOSPITALES
    public function index(){
      $data["listado"]=$this->Tipo->consultarTodos();
      $this->load->view("header");
      $this->load->view("tipos/index",$data);
    }
    //eliminacion de hospitales recibiendo el id por get
    public function borrar($id){
      $this->Tipo->eliminar($id);
      $this->session->set_flashdata("confirmacion","Tipo eliminado");
      redirect("tipos/index");
    }
    //renderizacion de formulario nuevo hospitales
    public function nuevo(){
      $this->load->view("header");
      $this->load->view("tipos/nuevo");
    }
    //cpturando datos e insertando en hospitales
    public function guardar(){
      $datosNuevo=array(
        "tipo"=>$this->input->post("tipo")
      );
      $this->Tipo->insertar($datosNuevo);
      $this->session->set_flashdata("confirmacion","Tipo guardado exitosamente");
      redirect('tipos/index');
    }
    //Renderizar el formulario de edicion
    //Renderizar el formulario de edicion
    public function editar($id){
      $data["editar"]=$this->Tipo->obtenerPorId($id);
      $this->load->view("header");
      $this->load->view("tipos/editar",$data);
    }


    //cpturando datos e insertando en hospitales
    public function actualizar(){
        $id=$this->input->post("id");
        $datos=array(
          "tipo"=>$this->input->post("tipo")
        );
        $this->Tipo->actualizar($id,$datos);
        $this->session->set_flashdata("confirmacion",
        "Tipo actualizado exitosamente");
        redirect('tipos/index');
      }
  }//Cierre de  la clase
?>
