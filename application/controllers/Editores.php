<?php

class Editores extends CI_Controller
{
  function __construct()
  {
      parent::__construct();
      $this->load->model("Editor");

      // deshabilitando errores y advertencias de PHP
      error_reporting(0);
      # EXPORTAR MODELO ARTICULO
          $this->load->model("Articulo");
          error_reporting(0);
  }

  public function index()
  {
      $data["listadoEditores"] = $this->Editor->consultarTodos();
      $this->load->view("header");
      $this->load->view("editores/index", $data);

  }

  public function borrar($id_ed)
  {
      $this->Editor->eliminar($id_ed);
      $this->session->set_flashdata("confirmacion", "Editor eliminada correctamente");

      redirect("editores/index");
  }

  public function nuevo()
  {
    # LLAMAR LOS ARTICULOS AGREGADAS
      $data["listado"] = $this->Articulo->consultarTodos();
      $this->load->view("header");
      $this->load->view("editores/nuevo",$data);

  }

  public function guardarEditor()
  {


      $datosNuevoEditor = array(
          "nombre" => $this->input->post("nombre"),
          "apellidos" => $this->input->post("apellidos"),
          "area" => $this->input->post("area"),
          # INCORPORANDO ID DE ARTICULO
          "id_art" => $this->input->post("id_art"),

      );

      // Insertar el nuevo registro en la base de datos
      $this->Editor->insertar($datosNuevoEditor);


      // Redireccionar a la página de listado de agencias
      redirect('editores/index');
  }

  public function editar($id)
  {
      $data["editorEditar"] = $this->Editor->obtenerPorId($id);
      # LLAMANDO ARTICULOS AGREGADAS
      $data["articulos"] = $this->Articulo->consultarTodos();
      $this->load->view("header");
      $this->load->view("editores/editar", $data);

  }


  public function actualizarEditor()
  {
      $id_ed = $this->input->post("id_ed");

      $datosEditar = array(
          "nombre" => $this->input->post("nombre"),
          "apellidos" => $this->input->post("apellidos"),
          "area" => $this->input->post("area"),
          // Llama al articulo
          "id_art" => $this->input->post("id_art"),
      );

      // Llama al método actualizar en el modelo Editor
      $this->Editor->actualizar($id_ed, $datosEditar);

      $this->session->set_flashdata("confirmacion", "Editor actualizada exitosamente");
      redirect('editores/index');
  }




}

 ?>
