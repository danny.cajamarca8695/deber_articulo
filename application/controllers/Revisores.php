<?php
class Revisores extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Revisor");


        // Habilitar el reporte de errores
        error_reporting();
    }

    public function index()
    {
        $data["listadoRevisores"] = $this->Revisor->consultarTodos();
        $this->load->view("header");
        $this->load->view("revisores/index", $data);
        $this->load->view("footer");
    }

    public function borrar($id_rev)
    {
        $this->Revisor->eliminar($id_rev);
        $this->session->set_flashdata("confirmacion", "Revisor eliminado correctamente");
        redirect("revisores/index");
    }

    public function nuevo()
    {
        $data["revisores"] = $this->Revisor->consultarTodos();
        $this->load->view("header");
        $this->load->view("revisores/nuevo", $data);
        $this->load->view("footer");
    }

    public function guardarRevisor()
    {
        // Proceso de subida de archivo (se puede agregar validación de archivo aquí)

        $datosNuevoRevisor = array(
            "nombres" => $this->input->post("nombres"),
            "apellidos" => $this->input->post("apellidos"),
            "area_espec" => $this->input->post("area_espec")
        );

        $this->Revisor->insertar($datosNuevoRevisor);
        $this->session->set_flashdata("confirmacion", "Coautor guardado exitosamente");
        redirect('revisores/index');
    }

    public function editar($id_rev)
    {
        $data["revisorEditar"] = $this->Revisor->obtenerPorId($id_rev);
        $this->load->view("header");
        $this->load->view("revisores/editar", $data);
        $this->load->view("footer");
    }

    public function actualizarRevisor()
    {
        $id_rev = $this->input->post("id_rev");

        // Proceso de subida de archivo (se puede agregar validación de archivo aquí)

        $datosRevisor = array(

            "nombres" => $this->input->post("nombres"),
            "apellidos" => $this->input->post("apellidos"),
            "area_espec" => $this->input->post("area_espec")
        );

        $this->Revisor->actualizar($id_rev, $datosRevisor);
        $this->session->set_flashdata("confirmacion", "Revisor actualizado exitosamente");
        redirect('revisores/index');
    }
}
?>
